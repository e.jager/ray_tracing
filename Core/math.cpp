#include <limits>
#include <cmath>
#include "math.hpp"
#include "vector.hpp"

using namespace rt;

double solve2(double a, double b, double c){
  double delta  = pow(b, 2.) - 4.*a*c;
  double t;
  if (delta > 0){
    double sqrt_delta = sqrt(delta);
    double t1 = (-b - sqrt_delta)/(2.*a);
    double t2 = (-b + sqrt_delta)/(2*a);
    if (t1 > 0){
        if (t2 > 0){
  	t = std::min(t1,t2);
      }
      else{
	t = t1;
      }
    }
    else{
      if (t2 > 0){
	t = t2;
      }
      else{
	t = infinity;
      }
    }
  }
  else{
    t = infinity;
  }
  return t;
}

vector project_plane(vector u, vector normal){
  vector n = normal.unit();
  return u - ((u|n)*n);
}

vector project_line(vector u, vector direction){
  vector d = direction.unit();
  return ((u|d)*d);
}
