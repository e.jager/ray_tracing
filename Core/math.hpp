/*!
 * \file math.hpp
 * \brief Cone class
 * \author Vidal Attias, Clément Legrand-Duchesne, Valentin Taillandier
 * \version 0.1
 */

#pragma once

#include <algorithm>
#include <limits>
#include <cmath>
#include "vector.hpp"

using namespace rt;

/** \brief Adding the value 'infinity' to doubles */
const double infinity = std::numeric_limits<double>::infinity();

 /*!
   * \fn double solve2(double a, double b, double c)
   *
   * \brief Trinominal equation solver
   *
   * \param a The second degree coeficient
   * \param b The first degree coefficent
   * \param c The last coefficient

   * \return t The first strictly positiv simple square root of the
   * polynom.  
   * \warnig If there are none or one root or only negativ
   * roots, the returned value will bee infinity.
   */  
double solve2(double a, double b, double c);

 /*!
   * \fn vector project_plane(vector u, vector normal)
   *
   * \brief Projection on a plane
   *
   * \param u The vector to project
   * \param normal A vector normal to the plane

   * \return The projection of u the the plane orthogonla to normal 
   */  
vector project_plane(vector u, vector normal);


 /*!
   * \fn vector project_line(vector u, vector direction)
   *
   * \brief Projection on a line
   *
   * \param u The vector to project
   * \param direction A vector directing the line
   *
   * \return The projection of u the the line directed by direction 
   */
vector project_line(vector u, vector direction);
