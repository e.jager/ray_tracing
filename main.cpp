/*!
 * \file main.cpp
 * \brief 3 Dimension picture generator using ray tracing
 * \author Vidal Attias, Clément Legrand-Duchesne, Valentin Taillandier
 * \version 0.1
 */

#include <iostream>
#include <unistd.h>
#include <fstream>
#include <cassert>
#include <vector>

#include "image.hpp"
#include "screen.hpp"
#include "color.hpp"
#include "scene.hpp"

#include "sphere.hpp"
#include "disk.hpp"
#include "cylinder.hpp"
#include "rectangle.hpp"
#include "cuboid.hpp"
#include "cone.hpp"
#include "plane.hpp"

#include "camera.hpp"
#include "vector.hpp"
#include "engine.hpp"

using namespace std;


int main(){
        int definition = 1;

        rt::screen s(640*definition,480*definition);

        Camera cam = Camera(rt::vector(320.0, 200.0, -300.0),
                            rt::vector(0.0, 0.0, 1.0));
        Scene scene = Scene(cam);

        float size = 1.;

        // Don't forget to turn off the light when leaving !
        Light lamp1 = Light(rt::vector(600.,100.,-300.),color::WHITE);
        scene.add_light(&lamp1);

        unsigned int demo;
        unsigned int nb_demo = 3;
        cout << "Which demo would you like to see ?" << endl;
        cin >> demo;
        while (demo >= nb_demo) {
                cout << "Only " << nb_demo << " demo are available for the\
                    moment. Which demo would you like to see ?"<< endl;
                cin >> demo;
        }
        switch (demo)
        {
        case 0:
        {
                rt::vector tree_base = rt::vector(340.,325.,50.)*size;
                scene.add_tree(200.,tree_base);
                break;
        }
        case 1:
        {
                rt::vector table_center = rt::vector(340.,325.,250.)*size;
                scene.add_pool(size,table_center);
                break;
        }
        case 2:
        {
                scene.add_shading();
        }
        }

        Engine engine= Engine(640, 480);

        engine.render_scene(scene, definition);

        clog << "Waiting for QUIT event..." << endl;
        while(not s.wait_quit_event()) {}

        return 0;
}
