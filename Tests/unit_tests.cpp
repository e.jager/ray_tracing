#include "vector_tests.hpp"
#include "color_tests.hpp"
#include "image_tests.hpp"
#include "math_tests.hpp"

int main(){
    test_vector();
    test_color();
    test_image();
    return 0;
}
