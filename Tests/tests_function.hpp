#pragma once
#ifndef PRINT_RESULT
#define PRINT_RESULT

#include <iostream>
#include <string>

void print_result(std::string message, bool test);
#endif
