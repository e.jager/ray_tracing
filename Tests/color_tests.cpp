#include <iostream>
#include <string>
#include "../Screen/color.hpp"
#include "tests_function.hpp"

using namespace rt;

void test_color(){
        color white_color = color();
        color color_construct = color(123,32,210);
        color color_construct_alpha = color(123, 32, 210, 50);
        color color_copy_construct = color(color_construct_alpha);


        bool test_white = (white_color.get_red() == 0
                           && white_color.get_green() == 0
                           && white_color.get_blue() == 0
                           && white_color.get_alpha() == 0);

        bool test_color_construct =
                (color_construct.get_red() == 123
                 && color_construct.get_green() == 32
                 && color_construct.get_blue() == 210
                 && color_construct.get_alpha() == 255);

        bool test_color_construct_alpha =
                (color_construct_alpha.get_red() == 123
                 && color_construct_alpha.get_green() == 32
                 && color_construct_alpha.get_blue() == 210
                 && color_construct_alpha.get_alpha() == 50);

        bool test_copy_construct = (color_copy_construct.get_red() == 123
                                    && color_copy_construct.get_green() == 32
                                    && color_copy_construct.get_blue() == 210
                                    && color_copy_construct.get_alpha() == 50);



        std::cout << std::endl << std::endl
                  << "*** Tests of color class" << std::endl;
        print_result("Color white constructor", test_white);
        print_result("Color RGB constructor", test_color_construct);
        print_result("Color RGBA constructor", test_color_construct_alpha);
        print_result("Color copy color", test_copy_construct);
}
