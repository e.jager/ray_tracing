#include <iostream>
#include <string>
#include "../Screen/vector.hpp"
#include "tests_function.hpp"
#include <cmath>

using namespace rt;

void print_vector(vector a){
        std::cout << a.x << " " << a.y << " " << a.z << std::endl;
}

void test_vector(){
        vector a = vector(1,2,3);
        vector a_bis = vector(1,2,3);
        vector b = vector(4,5,6);
        vector vect_addition = a + b;
        vector vect_substract = a - b;
        vector vect_vectorial_product = a^b;
        double scalar_product = a|b;
        double norm = a.norm();
        vector unit = a.unit();
        vector scalar_multiplication = 3 * a;


        vector expected_addition = vector(1+4, 2+5, 3+6);
        vector expected_substract = vector(1-4, 2-5, 3-6);
        vector expected_vectorial_product = vector(-3, 6, -3);
        double expected_scalar_product = 32;
        double expected_norm = std::sqrt(1*1 + 2*2 + 3*3);
        vector expected_unit = vector(1./norm, 2./norm, 3./norm);
        vector expected_scalar_multiplication = vector(3, 6, 9);


        std::cout << "*** Tests of vector structure ***" << std::endl;


        print_result("Vector equality", a == a_bis);
        print_result("Vector addition", vect_addition == expected_addition);
        print_result("Vector substraction",
                     vect_substract == expected_substract);
        print_result("Vector vectorial product",
                     vect_vectorial_product == expected_vectorial_product);
        print_result("Vector scalar product",
                     scalar_product == expected_scalar_product);
        print_result("Vector norm", norm == expected_norm);
        print_result("Vector unit", unit == expected_unit);
        print_result("Vector scalar multiplication",
                     scalar_multiplication == expected_scalar_multiplication);
}
