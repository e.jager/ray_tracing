#include "tests_function.hpp"

void print_result(std::string message, bool test){
    if(test){
        std::cout << "\"" << message << "\" has passed." << std::endl;
    }
    else{
        std::cout << "\"" << message << "\" has failed." << std::endl;
    }
}
