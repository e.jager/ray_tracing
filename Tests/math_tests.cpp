#include "math_tests.hpp"
#include "../Core/math.hpp"
#include "../Tests/tests_function.hpp"

void test_math(){

    //Test: Infinity definition
    print_result("Infinity definition", infinity == infinity2);

    std::cout << infinity2 + 1. << std::endl;

    //Test: Polynomial solver 1
    double a = 1.;
    double b = 0.;
    double c = 0.;
    double result1 = solve2(a, b, c);
    double expected_result1 = infinity2;
    print_result("Polynomial solver: test 1", result1 == expected_result1);


    //Test: Polynomial solver 2
    double d = 1.;
    double e = 0.;
    double f = -1.;
    double result2 = solve2(d, e, f);
    double expected_result2 = 1. ;
    print_result("Polynomial solver: test 2", result2 == expected_result2);

    //Test: Polynomial solver 3
    double g = 1.;
    double h = 0.;
    double i = 1.;
    double result3 = solve2(g,h, i);
    double expected_result3 = infinity2;
    print_result("Polynomial solver: test 3", result3 == expected_result3);

}
