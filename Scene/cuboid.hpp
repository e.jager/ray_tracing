/*!
 * \file cuboid.hpp
 * \brief Cuboid class
 * \author Vidal Attias, Clément Legrand-Duchesne, Valentin Taillandier
 * \version 0.1
 */

#pragma once

#include "color.hpp"
#include "vector.hpp"
#include "hit.hpp"
#include "ray.hpp"
#include "object.hpp"
#include "../Core/math.hpp"
#include "rectangle.hpp"
#include <utility>
#include <iostream>
#include <utility>
#include <assert.h>
#include <cmath>


using namespace rt;

/*! \class Cuboid
 * \brief Class representing a cuboid object.
 *
 * The cuboid class represents a cuboid through the position of its
 * center, two vectors direction, a height parameter and its
 * color. */

class Cuboid : public Object {
private:
  vector position;     /*!< Vector representing the position. */
  vector direction1;   /*!< Vector leading from the center of the cuboid
			 to the center of one side s1. */
  vector direction2;   /*!< Vector leading from the center of the cuboid
			 to the center of a side s2 adjacent to the
			 first one. */
  
  double height;   /*!< Double representing the length of the vector
		     leading from the center of the cuboid to the center
		     of a side orthogonal to s1 and s2. */
  color col;   /*!< Color of the cuboid. */
  
  std::vector<Rectangle> rectangles;   /*!< std::vector containing the
                                         sides of the cuboid. */
  
  /*!
   * \fn std::pair<double, int> send_which(Ray r) const
   *
   * \brief Finds the side and the distance to the intersection of the
   * ray with the cuboid.
   *
   * \param r A ray.
   *
   * \return A pair (double t, int s). t being the distance from the
   * origin of the ray to the first side of the cuboid, s the
   * aforesaid side. If the ray doesn't cross the cuboid then t =
   * infinity and s = 0.
   *
   */  
  std::pair<double, int> send_which(Ray r) const;

public:
  
  /*!
   * \brief Constructor: builds a cuboid.
   *
   * \param p The position of the center of the cuboid.
   * \param dir1 The first direction.
   * \param dir2 The second direction.
   * \param hei The height.
   * \param co The color.
   *
   * \warning The directions given in parameters has to be
   * orthogonal. Otherwise direction2 will be set to the projection of
   * dir2 on the plane orthogonal to dir1
   */
  Cuboid(vector p, vector dir1, vector dir2, double hei, color co);
  
  /*! \fn vector get_position() const
   * \brief Gets the position of the center.
   */
  vector get_position() const;
  
  /*! \fn vector get_first_direction() const;
   * \brief Gets the first direction
   */
  vector get_first_direction() const;
  
  
  /*! \fn vector get_second_direction() const
   * \brief Gets the second direction.
   */
  vector get_second_direction() const;
  
  /*!\fn double get_height() const
   * \brief Gets the height.
   */
  double get_height() const;
  
  /*! \fn color get_color() const
   * \brief Gets the color.
   */
  color get_color() const;
  
  /*!
   * \fn  double send(Ray r) const
   *
   * \brief Finds the distance to the intersection of the
   * ray with the cuboid.
   *
   * \param r A ray.
   *
   * \return A double t, t being the distance from the origin of the
   * ray to the first side of the cuboid in the ray direction. t is
   * strictly positiv. If the ray doesn't cross the cuboid then t =
   * infinity.
   */
  double send(Ray r) const;
  
  /*!
   * \fn  Hit intersect(Ray r, double t) const
   *
   * \brief Creates the resulting Hit.
   *
   * \param r A ray.
   * \param t A double.
   *
   * \return A Hit.
   *
   * Creates the Hit corresponding to the intersection of the cuboid
   * and the ray at a distance t times the direction of the ray from
   * the origin of the ray.
   * Computes the normal vector to the side crossed and its color.
   */
  Hit intersect(Ray r, double t) const;
};
