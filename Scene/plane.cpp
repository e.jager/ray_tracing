#include "plane.hpp"
#include <iostream>
#include "../Core/math.hpp"
#include "assert.h"

using namespace rt;

Plane::Plane(vector p, vector n ,color co){
    normal = n.unit();
    position = p;
    col = co;
}

vector Plane::get_normal() const{
  return normal;
}

vector Plane::get_position() const{
    return position;
}

color Plane::get_color() const{
    return col;
}

double Plane::send(Ray r) const {
    double t;
    vector direction = r.get_direction().unit();
    vector origin = r.get_origin();

    if( (direction|normal)*(direction|normal) != 0){
        t = ((position-origin)|normal)/(direction|normal);
    }
    else{
        t = infinity;
    }

    if(t<0){
        t = infinity;
    }
    return t;
}

Hit Plane::intersect(Ray r, double t) const {
  assert(t > 0);
  double intersect_x = r.get_origin().x + t*r.get_direction().x;
  double intersect_y = r.get_origin().y + t*r.get_direction().y;
  double intersect_z = r.get_origin().z + t*r.get_direction().z;
  vector normal_vector;
  if((normal|r.get_direction()) != 0){
      normal_vector = -(normal|r.get_direction())*normal;
  }
  else{
      normal_vector = normal;
  }

  vector intersect_point = vector(intersect_x, intersect_y, intersect_z);
  return Hit(r, intersect_point, normal_vector, col);
}
