#include "disk.hpp"
#include <iostream>
#include "../Core/math.hpp"

using namespace rt;

Disk::Disk(vector p, vector n, double r, color co){
        normal = n.unit();
        position = p;
        radius = r;
        col = co;
}

vector Disk::get_normal() const {
        return normal;
}

vector Disk::get_position() const {
        return position;
}

color Disk::get_color() const {
        return col;
}

double Disk::get_radius() const {
        return radius;
}


double Disk::send(Ray r) const {
        double t;
        vector direction = r.get_direction().unit();
        vector origin = r.get_origin();

        if( (direction|normal)*(direction|normal) != 0) {
                t = ((position-origin)|normal)/(direction|normal);
                if(t >= 0) {
                        if((position-origin - t*direction).norm() <= radius) {
                                return t;
                        }
                        else{
                                return infinity;
                        }
                }
                else{
                        return infinity;
                }
        }
        else{
                return infinity;
        }
}

Hit Disk::intersect(Ray r, double t) const {
        vector direction = r.get_direction().unit();
        vector origin = r.get_origin();
        double intersect_x = origin.x + t*direction.x;
        double intersect_y = origin.y + t*direction.y;
        double intersect_z = origin.z + t*direction.z;
        vector normal_vector;
        if((normal|direction) != 0) {
                normal_vector = -(normal|direction)*normal;
        }
        else{
                normal_vector = normal;
        }

        vector intersect_point = vector(intersect_x, intersect_y, intersect_z);
        return Hit(r, intersect_point, normal_vector, col);
}
