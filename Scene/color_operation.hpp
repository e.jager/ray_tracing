/*!
 * \file color_operation.hpp
 * \brief Arithmetical operations on colors
 * \author Vidal Attias, Clément Legrand-Duchesne, Valentin Taillandier
 * \version 0.1
 */
#pragma once

#include "color.hpp"
#include <algorithm>

using namespace rt;


const color brown = color(157,52,12);       /*!< color brown. */
const color orange = color(255,127,0);      /*!< color orange. */
const color yellow = color(255,238,0);      /*!< color yellow. */
const color dark_green = color(0,86,27);    /*!< color dark green. */
const color grass_green = color(60,200,20); /*!< color grass green. */
const color dark_blue = color(34,66,124);   /*!< color dark blue. */


/*! \fn color operator +(const color& c1, const color& c2)
 * \brief Sums two colors
 * \param c1 A color
 * \param c2 A second color
 *
 * The color returned it the sum component by component of the two colors.
 *
 * \warning Since the color components are bounded by 255, the sum of
 * the two components are truncated at 255.
 */
color operator +(const color& c1, const color& c2);

/*! \fn color operator -(const color& c1, const color& c2)
 * \brief Substract one color from another
 * \param c1 A color
 * \param c2 The color to substract
 *
 * The color returned it the substraction component
   by component of the two colors.
 *
 * \warning Since the color components are low-bounded by 0, the
 * substraction of the two components are truncated at 0.
 */
color operator -(const color& c1, const color& c2);

/*! \fn color operator *(const color& c1, const float& lambda)
 * \brief Multiplicaion with a scalar.
 * \param c A color
 * \param lambda The scalar
 *
 * The color returned it the multiplication component by component the
 * color with the scalar.
 *
 * \warning Since the color components are bound by 255, the returned
 * components are truncated at 255
 */
color operator *(const color& c, const float& lambda);

/*! \fn color operator *(const color& c1, const float& lambda)
 * \brief Multiplicaion with a scalar.
 * \param lambda The scalar
 * \param c A color
 *
 * The color returned it the multiplication component by component the
 * color with the scalar.
 *
 * \warning Since the color components are bound by 255, the returned
 * components are truncated at 255
 */
color operator *(const float& lambda, const color& c);

/*! \fn color fuse(const color& c1, const color& c2, const color& c3,
   const color& c4);
 * \brief Fuses four colors into one.
 * \param c1 A color
 * \param c2 A color
 * \param c3 A color
 * \param c4 A color
 *
 * The color returned is the mean of the four given colors.
 */
color fuse(const color& c1, const color& c2, const color& c3, const color& c4);


/*! \fn color comp(const color c)
 * \brief Complementary of the color.
 * \param c A color
 *
 * The color returned is the substraction of c from whit light
 * (255,255,255).
 */
color comp(const color& c);
