#include "light.hpp"

using namespace rt;

Light::Light(vector p, color c){
        pos = p;
        col = c;
}

vector Light::get_position() const {
        return pos;
}

color Light::get_color() const {
        return col;
}

color Light::apply(Hit r){

        double dp_light = (r.get_normal().unit()|
                           (pos - r.get_point()).unit());
        //double dp_camera =
        //(r.get_normal().unit()|(r.get_ray().get_direction().unit()));
        if (dp_light > 0) {
                color pixel_col = r.get_color() - comp(col);
                return color(dp_light*pixel_col.get_red(),
                             dp_light*pixel_col.get_green(),
                             dp_light*pixel_col.get_blue());

        }
        else{
                return color::BLACK;

        }


}
