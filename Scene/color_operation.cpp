#include "color_operation.hpp"

using namespace rt;


color operator +(const color& c1, const color& c2)
{
        unsigned short int red1 = (unsigned short int) c1.get_red();
        unsigned short int green1 = (unsigned short int) c1.get_green();
        unsigned short int blue1 = (unsigned short int) c1.get_blue();
        unsigned short int red2 = (unsigned short int) c2.get_red();
        unsigned short int green2 = (unsigned short int) c2.get_green();
        unsigned short int blue2 = (unsigned short int) c2.get_blue();
        unsigned char red = (unsigned char) std::min(red1 + red2, 255);
        unsigned char green = (unsigned char) std::min(green1 + green2, 255);
        unsigned char blue = (unsigned char) std::min(blue1 + blue2, 255);
        return color(red, green, blue);
}

color operator -(const color& c1, const color& c2)
{
        unsigned short int red1 = (unsigned short int) c1.get_red();
        unsigned short int green1 = (unsigned short int) c1.get_green();
        unsigned short int blue1 = (unsigned short int) c1.get_blue();
        unsigned short int red2 = (unsigned short int) c2.get_red();
        unsigned short int green2 = (unsigned short int) c2.get_green();
        unsigned short int blue2 = (unsigned short int) c2.get_blue();
        unsigned char red;
        if(red1 > red2) {
                red = (unsigned char) red1 - red2;
        }
        else{
                red = 0;
        }
        unsigned char green;
        if(green1 > green2) {
                green = (unsigned char) green1 - green2;
        }
        else{
                green = 0;
        }
        unsigned char blue;
        if(blue1 > blue2) {
                blue = (unsigned char) blue1 - blue2;
        }
        else{
                blue = 0;
        }
        return color(red, green, blue);
}

color operator *(const color& c, const float& lambda)
{
        unsigned char red = (unsigned char) (((float) c.get_red()) * lambda);
        unsigned char
                green = (unsigned char) (((float) c.get_green()) * lambda);
        unsigned char blue = (unsigned char) (((float) c.get_blue()) * lambda);
        return color(red, green, blue, c.get_alpha());
}

color operator *(const float& lambda, const color& c)
{
        return c*lambda;
}

color fuse(const color& c1, const color& c2, const color& c3, const color& c4)
{
        float red1 = (float) c1.get_red();
        float green1 = (float) c1.get_green();
        float blue1 = (float) c1.get_blue();

        float red2 = (float) c2.get_red();
        float green2 = (float) c2.get_green();
        float blue2 = (float) c2.get_blue();

        float red3 = (float) c3.get_red();
        float green3 = (float) c3.get_green();
        float blue3 = (float) c3.get_blue();

        float red4 = (float) c4.get_red();
        float green4 = (float) c4.get_green();
        float blue4 = (float) c4.get_blue();

        float red = (red1 + red2 + red3 + red4)/4;
        float green = (green1 + green2 + green3 + green4)/4;
        float blue = (blue1 + blue2 + blue3 + blue4)/4;

        return color(red, green, blue, c1.get_alpha());

}

color comp(const color& c){
        return (color::WHITE - c);
}
