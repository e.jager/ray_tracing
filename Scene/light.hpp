/*!
 * \file light.hpp
 * \brief Light class
 * \author Vidal Attias, Clément Legrand-Duchesne, Valentin Taillandier
 * \version 0.1
 */

#pragma once

#include "vector.hpp"
#include "color.hpp"
#include "ray.hpp"
#include "hit.hpp"
#include "color_operation.hpp"

using namespace rt;

/*! \class Light
 * \brief Class representing a light source.
 *
 * A light is set by its position and its color.
 */
class Light {

private:
vector pos;   /*!< vector representing the position of the light. */
color col;    /*!< color representing the color of the light. */

public:

/*!
 * \brief Constructor: Creates a light source object.
 *
 * \param p The position
 * \param c The color
 */
Light(vector p, color c);

/*! \fn vector get_position() const;
 * \brief Returns the position of the light.
 */
vector get_position() const;

/*! \fn color get_color() const;
 * \brief Returns the color of the light.
 */
color get_color() const;

/*! \fn color apply(Hit r);
 * \brief Applies the light color to the ray of the Hit object.
 * \para r The Hit object which contains the ray to set the color of the light.
 */
color apply(Hit r);
};
