/*!
 * \file cone.hpp
 * \brief Cone class
 * \author Vidal Attias, Clément Legrand-Duchesne, Valentin Taillandier
 * \version 0.1
 */


#pragma once

#include "color.hpp"
#include "vector.hpp"
#include "hit.hpp"
#include "ray.hpp"
#include "object.hpp"
#include "../Core/math.hpp"
#include "disk.hpp"
#include <utility>


using namespace rt;

/*! \class Cone
 * \brief Class representing a cone object.
 *
 * The Cone class represents a cone through the position of the center
 * of its base, the vector leading from this center to the peak of the
 * cone, the radius of the cone, its color and the base of the
 * cone.
 */
class Cone : public Object {
private:
  vector position; /*!< Vector representing the position. */
  vector axis; /*!< Vector leading from the center of the cuboid to
                  the peak. */
  double radius; /*!< Radius of the base*/
  color col; /*!< Color of the cone*/
  Disk base; /*!< Base of the cone*/

  /*!
   * \fn std::pair<double, char> send_which(Ray r) const
   *
   * \brief Finds the side and the distance to the intersection of the
   * ray with the cone.
   *
   * \param r A ray.
   *
   * \return A pair (double t, char c). t being the distance from the
   * origin of the ray to the first point of the cone reached, c
   * refering to the part of the cone reached. If the ray doesn't cross the
   * cone then t = infinity and c is random.
   *
   */  
  std::pair<double,char> send_which(Ray r) const;

public:

  /*!
   * \brief Constructor: builds a cone.
   *
   * \param p The position of the center of the base.
   * \param a The vector leading from p to the peak.
   * \param r The radius.
   * \param co The color.
   *
   */
  Cone(vector p, vector a, double r, color co);

  /*!\fn vector get_position() const
   * \brief Gets the position of the center of the base.
   */
  vector get_position();

  /*!\fn vector get_axis() const
   * \brief Gets the axis component.
   */
  vector get_axis();

  /*!\fn double get_radius() const
   * \brief Gets the radius.
   */
  double get_radius();

  /*!\fn color get_color() const
   * \brief Gets the color of the cone.
   */
  color get_color();

  /*!
   * \fn  double send(Ray r) const
   *
   * \brief Finds the distance to the intersection of the
   * ray with the cone.
   *
   * \param r A ray.
   *
   * \return A double t, t being the distance from the origin of the
   * ray to the first point of the cone reached. If the ray doesn't
   * cross the cone then t = infinity.
   */
  double send(Ray r) const;

  /*!
   * \fn  Hit intersect(Ray r, double t) const
   *
   * \brief Creates the resulting Hit.
   *
   * \param r A ray.
   * \param t A double.
   *
   * \return A Hit.
   *
   * Creates the Hit corresponding to the intersection of the cone
   * and the ray at a distance t times the direction of the ray from
   * the origin of the ray.
   * Computes the normal vector to the side crossed and its color.
   */
  
  Hit intersect(Ray r, double t) const;
};
