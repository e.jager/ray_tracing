#pragma once

#include <iostream>
#include <cassert>

#include "vector.hpp"

using namespace rt;

class Camera {

private:
vector position;
vector orientation;
/* For simplification purposes, we consider that the camera must be
   //horizontal, that is, the first coordinate must be 0 */

public:
Camera() :
        position(320.0, 240.0, -300.0),
        orientation(0.0, 0.0, 1.0)
{
}

Camera(vector _position, vector _orientation) {
        position = _position;
        vector new_orientation =
                vector(0, _orientation.y, _orientation.z);
        /*
         * The x coordinate must be zero
         * and at least one other coordinate
         * must be different from zero
         */
        assert(new_orientation.norm() != 0);
        orientation = new_orientation.unit();
}

vector get_position() const {
        return position;
}

vector get_orientation() const {
        return orientation;
}

void set_position(vector _position) {
        position = _position;
}

void set_orientation(vector _orientation) {
        vector new_orientation =
                vector(0, _orientation.y, _orientation.z);
        /*
         * The x coordinate must be zero
         * and at least one other coordinate
         * must be different from zero
         */
        assert(new_orientation.norm() != 0);
        orientation = new_orientation.unit();
}
};
