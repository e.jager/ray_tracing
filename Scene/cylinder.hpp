/*!
 * \file cylinder.hpp
 * \brief Cylinder class
 * \author Vidal Attias, Clément Legrand-Duchesne, Valentin Taillandier
 * \version 0.1
 */

#pragma once

#include <utility>
#include <assert.h>
#include <iostream>

#include "color.hpp"
#include "vector.hpp"
#include "hit.hpp"
#include "ray.hpp"
#include "object.hpp"
#include "../Core/math.hpp"
#include "disk.hpp"

using namespace rt;

/*! \class Cylinder
 * \brief Class representing a cylindrical object.
 *
 * The Cylinder class represents a cylinder through the position of
 * its center, a vector axis leading from one side to the other, a
 * radius, and a color.
 */
class Cylinder : public Object {
private:
vector position;   /*!< Vector representing the position of the center
                      of the lower side. */
vector axis;   /*!< Vector leading from one side to the other. */
double radius;   /*!< Radius of the cylinder. */
color col;    /*!< Color of the cylinder. */
Disk bottom;   /*!< Lower side of the cylinder. */
Disk top;   /*!< Upper side of the cylinder. */

/*!
 * \fn std::pair<double, char> send_which(Ray r) const
 *
 * \brief Finds the part and the distance to the intersection of the
 * ray with the cylinder.
 *
 * \param r A ray.
 *
 * \return A pair (double t, char p). t being the distance from the
 * origin of the ray to the first side of the cuboid, p the
 * aforesaid part. If the ray doesn't cross the cuboid the t =
 * infinity and p is random.
 *
 */
std::pair<double, char> send_which(Ray r) const;

public:

/*!
 * \brief Constructor: builds a cylinder.
 *
 * \param p The position of the center of the lower side.
 * \param a The vector axis.
 * \param r The radius.
 * \param co The color.
 */
Cylinder(vector p, vector a, double r, color co);

/*! \fn vector get_position() const
 * \brief Gets the position of the center of the lower side.
 */
vector get_position() const;

/*! \fn vector get_axis() const
 * \brief Gets the axis.
 */
vector get_axis() const;

/*! \fn vector get_radius() const
 * \brief Gets the radius of the cylinder.
 */
double get_radius() const;

/*! \fn color get_color() const
 * \brief Gets the color of the cylinder.
 */
color get_color() const;

/*!
 * \fn  double send(Ray r) const
 *
 * \brief Finds the distance to the intersection of the
 * ray with the cylinder.
 *
 * \param r A ray.
 *
 * \return A double t, t being the distance from the origin of the
 * ray to the first part of the cuboid crossed in thr ray
 * direction. t is strictly positiv. If the ray doesn't cross the
 * cylinder then t = infinity.
 */
double send(Ray r) const;

/*!
 * \fn  Hit intersect(Ray r, double t) const
 *
 * \brief Creates the resulting Hit.
 *
 * \param r A ray.
 * \param t A double.
 *
 * \return A Hit.
 *
 * Creates the Hit corresponding to the intersection of the cylinder
 * and the ray at a distance t times the direction of the ray from
 * the origin of the ray.
 * Computes the normal vector to the surface crossed and its color.
 */
Hit intersect(Ray r, double t) const;
};
