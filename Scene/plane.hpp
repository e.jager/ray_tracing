/*!
 * \file plane.hpp
 * \brief Plane class
 * \author Vidal Attias, Clément Legrand-Duchesne, Valentin Taillandier
 * \version 0.1
 */

#pragma once

#include "color.hpp"
#include "vector.hpp"
#include "hit.hpp"
#include "ray.hpp"
#include "object.hpp"

using namespace rt;

/*! \class Plane
 * \brief Class representing a plane.
 *
 * The Plane class represents a plane through the position of
 * one point of the plane, a vector normal, and a color.
 */
class Plane : public Object {
private:
vector position;   /*!< Vector representing the position of one point
                      of the plane */
vector normal;   /*!< Vector normal to the plane */
color col;   /*!< Color of the plane */

public:

/*!
 * \brief Constructor: builds a plane.
 *
 * \param p The position of one point.
 * \param n The normal vector.
 * \param co The color.
 */
Plane(vector p, vector n, color co);

/*! \fn vector get_position() const
 * \brief Gets the position of the point.
 */
vector get_position() const;

/*! \fn vector get_nomal() const
 * \brief Gets the normal vector.
 */
vector get_normal() const;

/*! \fn color get_color() const
 * \brief Gets the color of the plane.
 */
color get_color() const;

/*!
 * \fn  double send(Ray r) const
 *
 * \brief Finds the distance to the intersection of the
 * ray with the plane.
 *
 * \param r A ray.
 *
 * \return A double t, t being the distance from the origin of the
 * ray to the plane in the ray direction. t is strictly positiv. If
 * the ray doesn't cross the plane then t = infinity.
 */
double send(Ray r) const;

/*!
 * \fn  Hit intersect(Ray r, double t) const
 *
 * \brief Creates the resulting Hit.
 *
 * \param r A ray r.
 * \param t A double t.
 *
 * \return A Hit.
 *
 * Creates the Hit corresponding to the intersection of the plane
 * and the ray at a distance t times the direction of the ray from
 * the origin of the ray.
 * Computes the normal vector to the surface crossed and its color.
 */
Hit intersect(Ray r, double t) const;
};
