#include <iostream>
#include "sphere.hpp"
#include "../Core/math.hpp"
#include <cmath>

using namespace rt;

//Basic constructor -> type = 0
Sphere::Sphere(vector ce, double r, color co){
        strips = 0;
        center = ce;
        radius = r;
        col = co;
}

//Stripped constructor -> type = 1

Sphere::Sphere(vector ce, double r, color co, color co2, int s){
        center = ce;
        radius = r;
        col = co;
        col_strip = co2;
        strips = s;
}

vector Sphere::get_center() const {
        return center;
}

double Sphere::get_radius() const {
        return radius;
}

color Sphere::get_color(vector point) const {
        color co = color();
        if (strips == 0) {
                co = col;
        }
        else
        {
                int modulo =
                        ((int)(strips*std::abs(point.y - center.y) /radius))%2;
                if(modulo == 0) {
                        co = col;
                }
                else{
                        co = col_strip;
                }
        }
        return co;
}

double Sphere::send(Ray r) const {
        vector ac = center-r.get_origin();
        vector u = r.get_direction().unit();
        // coefficients of the polynome to solve:
        double a = u.norm()*u.norm();
        double b = -2*(ac|u);
        double c = ac.norm()*ac.norm() - radius*radius;
        double t = solve2(a,b,c);

        if(t<0) {
                t = infinity;
        } //redundant
        return t;
}

Hit Sphere::intersect(Ray r, double t) const {
        vector dir = r.get_direction().unit();
        vector orig = r.get_origin();

        double intersect_x = orig.x + t*dir.x;
        double intersect_y = orig.y + t*dir.y;
        double intersect_z = orig.z + t*dir.z;

        vector intersect_point = vector(intersect_x, intersect_y, intersect_z);
        vector normal_vector = (intersect_point - center).unit();
        return Hit(r, intersect_point, normal_vector,
                   this->get_color(intersect_point));
}
