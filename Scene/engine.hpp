/*!
 * \file engine.hpp
 * \brief Engine class
 * \author Vidal Attias, Clément Legrand-Duchesne, Valentin Taillandier
 * \version 0.1
 */


#pragma once

#include <vector>
#include "vector.hpp"
#include "screen.hpp"
#include "image.hpp"
#include "color.hpp"
#include "sphere.hpp"
#include "ray.hpp"
#include "hit.hpp"
#include "light.hpp"
#include "object.hpp"
#include "camera.hpp"
#include "scene.hpp"
#include "color_operation.hpp"


using namespace rt;

/*! \class Engine
 * \brief Class representing an engine which can
   compute a graphic from a Scene Object
 * A x4 antialiasing is used to compute each pixel.
 * color. */
class Engine {

private:
int width;      /*!< Int representing the width of the graphic. */
int height;     /*!< Int representing the height the graphic. */

/*! \fn color pixel_color(int i, int j, Scene scene)
 * \brief Computes the color of the pixel at (i,j).
 * The function also computes a reflexion by computing a new ray
   leaving the first object met
 * It makes an addition of color with a ponderation coefficient.
 * \param i An int representing the easting of the pixel.
 * \param j An int representing the northing ot the pixel.
 * \param scene A scene.
 */
color pixel_color(double i, double j, Scene scene);

public:

/*!
 * \brief Constructor: builds an engine.
 *
 * \param w The width of the graphic.
 * \param h The height of the graphic.
 */
Engine(int w, int h);
Engine();

/*! \fn int get_width();
 * \brief Returns the width of the graphic.
 */
int get_width();

/*! \fn int get_width();
 * \brief Returns the height of the graphic.
 */
int get_height();

/*! \fn void render_scene(Scene universe);
 * \brief Computes the color of each pixels of the graphic and print the
   picture.
 * \param Scene scene.
 */
void render_scene(Scene universe, int definition);
};
