/*!
 * \file hit.hpp
 * \brief Hit class
 * \author Vidal Attias, Clément Legrand-Duchesne, Valentin Taillandier
 * \version 0.1
 */


#pragma once

#include "vector.hpp"
#include "color.hpp"
#include "ray.hpp"

using namespace rt;

/*! \class Hit
 * \brief Class representing the event "a ray of light strikes an object".
 *
 * The Hit class represents the event "a ray strikes an object" by
 * the aforesaid ray, the point where the ray reaches the object, the
 * vector normal to the surface in this point and the color of the
 * object in this point. */
class Hit{

private:
  Ray gen; /*!< Current Ray of light */ 
  vector point; /*!< Point where the ray reaches the object */
  vector normal; /*!< Vector normal to the surface in this point */
  color col; /*!< the color of the object in this point */

public:

  /*!
   * \brief Constructor: creates a Hit.
   *
   * \param g The Ray considered.
   * \param p The the meeting point.
   * \param n The vector normal to the surface.
   * \param c The color of the object in this point.
   */
  Hit(Ray g, vector p, vector n, color c);

  /*! \fn Ray get_ray() const
   * \brief Gets the current Ray.
   */
  Ray get_ray();

  /*! \fn vector get_point() const
   * \brief Gets the position of the meeting point.
   */
  vector get_point();

  /*! \fn vector get_normal() const
   * \brief Gets the normal component of the Hit.
   */
  vector get_normal();

  /*! \fn color get_color() const
 * \brief Gets the color of the struck object
 */
  color get_color();
};
