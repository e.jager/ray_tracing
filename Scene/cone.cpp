#include <iostream>
#include "color.hpp"
#include "vector.hpp"
#include "hit.hpp"
#include "ray.hpp"
#include "object.hpp"
#include "../Core/math.hpp"
#include "disk.hpp"
#include <utility>
#include "cone.hpp"


using namespace rt;

Cone::Cone(vector p, vector a, double r, color co):
  base(Disk(p, a,r,co)){
  position = p;
  axis = a;
  radius = r;
  col = co;
}

vector Cone::get_position(){
  return position;
}

vector Cone::get_axis(){
  return axis;
}

double Cone::get_radius(){
  return radius;
}
color Cone::get_color(){
  return col;
}

std::pair<double,char>  Cone::send_which(Ray r) const{

    vector o = r.get_origin();
    vector d = r.get_direction();
    vector a = axis;
    vector p = position+axis;

    vector u = (o-p)+  a* (((p-o)|a)/pow(a.norm(),2)) ;
    vector v =  d - a*(    (d|a)/pow(a.norm(),2  )   );

    double alpha = ((p-o)|a)*radius/pow(a.norm(), 2);
    double beta = -(d|a)*radius/pow(a.norm(), 2);

    double aSolve = pow(v.norm(), 2)-pow(beta, 2);
    double bSolve = 2*( (u|v) - (alpha*beta) );
    double cSolve = pow(u.norm(), 2) - pow(alpha, 2);

    // t_con is the distance to the intersection with hull of the cone
    double t_con = solve2(aSolve, bSolve, cSolve);

    // lambda*radius is the radius of the circle orthogonal to axis in
    // the point of intersection
    double lambda = ( (p-o-t_con*d)|a)/pow(a.norm(), 2);

    if(lambda < 0 || lambda > 1){
      // the intersection with the ray is above or below the cone
      t_con = infinity;
    }

    // t_base is the distance to the intersection with the base
    double t_base = base.send(r);

    // WARNING: as t_cyl is the first positiv root, it is possible that
    // it corresponds to a point that above or below the bases while the
    // second root is a correct match. Resulting in a hole in the
    // cylinder That is actually not a serious matter because this hole
    // will be hidden by the disks in the eyes of the observater and
    // will not spoil the shades

    // t is the distance to the cone
    double t;
    // touched is the part of the cone which is reached
    char touched;
        
    if (t_con < t_base){
      t = t_con;
      touched = 'c';
    }
    else {
      t = t_base;
      touched = 'b';
    }
    const double t_final = t;
    const char touched_final = touched;
    return std::pair<double,char>(t_final, touched_final);
}

double Cone::send(Ray r) const{
  return send_which(r).first;
}

Hit Cone::intersect(Ray r, double t) const{
    double intersect_x = r.get_origin().x + t*r.get_direction().x;
    double intersect_y = r.get_origin().y + t*r.get_direction().y;
    double intersect_z = r.get_origin().z + t*r.get_direction().z;
    vector intersect_point = vector(intersect_x, intersect_y, intersect_z);
    vector normal_vector;
    if (send_which(r).second == 'c'){
      vector u = project_plane(intersect_point-position,axis).unit();
      return Hit(r,
		 intersect_point,
		 (u + axis.unit()*(radius/axis.norm())).unit(),
		 col);
    }

    return Hit(r, intersect_point, -1*axis, col);
}
