#pragma once

#include "vector.hpp"
#include "color.hpp"

using namespace rt;

class Ray {

private:
  vector orig;
  vector dir;
  color col;

public:
  Ray(vector o, vector d, color c);
  Ray(vector o, vector d);
  vector get_origin() const;
  vector get_direction() const;
  color get_color() const;
};
