#include "rectangle.hpp"
#include <iostream>
#include "assert.h"
#include "../Core/math.hpp"
#include <cmath>

using namespace rt;

Rectangle::Rectangle(vector p, vector dir1, vector dir2,color co){
        direction1 = dir1;
        direction2 = project_plane(dir2, dir1);
        normal = (dir1^dir2).unit();
        position = p;
        col = co;
}

vector Rectangle::get_first_direction() const {
        return direction1;
}

vector Rectangle::get_second_direction() const {
        return direction2;
}


vector Rectangle::get_normal() const {
        return normal;
}

vector Rectangle::get_position() const {
        return position;
}

color Rectangle::get_color() const {
        return col;
}

double Rectangle::send(Ray r) const {
        double t;
        vector direction = r.get_direction().unit();
        vector origin = r.get_origin();

        if( (direction|normal)*(direction|normal) != 0) {
                t = ((position-origin)|normal)/(direction|normal);
                vector intersect_point = origin + t*direction;
                vector distance = (intersect_point-position);
                if(t > 0) {
                        if ((project_line(distance, direction1).norm()
                             <= direction1.norm()) and
                            (project_line(distance, direction2).norm()
                             <= direction2.norm())) {
                                return t;
                        }
                        else{
                                return infinity;
                        }
                }
                else{
                        return infinity;
                }
        }
        else{
                return infinity;
        }

}

Hit Rectangle::intersect(Ray r, double t) const {
        assert(t > 0);
        double intersect_x = r.get_origin().x + t*r.get_direction().x;
        double intersect_y = r.get_origin().y + t*r.get_direction().y;
        double intersect_z = r.get_origin().z + t*r.get_direction().z;
        vector normal_vector;
        if((normal|r.get_direction()) != 0) {
                normal_vector = -(normal|r.get_direction())*normal;
        }
        else{
                normal_vector = normal;
        }

        vector intersect_point = vector(intersect_x, intersect_y, intersect_z);
        return Hit(r, intersect_point, normal_vector, col);
}
