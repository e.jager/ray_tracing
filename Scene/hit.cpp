#include "hit.hpp"

using namespace rt;

Hit::Hit(Ray g, vector p, vector n, color c) :
  gen(g), point(p), normal(n), col(c)
{}

Ray Hit::get_ray(){
  return gen;
}

vector Hit::get_point(){
  return point;
}

vector Hit::get_normal(){
  return normal;
}

color Hit::get_color(){
  return col;
}
