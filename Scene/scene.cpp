#include "scene.hpp"

using namespace rt;

// Scene::Scene(camera cam, light lamp);

Scene::Scene(Camera c) :
        cam(c) {
        //cam = Camera();
        //lamp = Light(cam.get_position(),color::WHITE);
        //objects = std::vector<Object*>;
}

Scene::Scene() :
        cam(Camera()) {
        //cam = Camera();
        //lamp = Light(cam.get_position(),color::WHITE);
        //objects = std::vector<Object*>;
}

Camera Scene::get_camera() const {
        return cam;
}

std::vector <Object*>  Scene::get_objects() const {
        return objects;
}

std::vector <Light*>  Scene::get_lights() const {
        return lights;
}

void Scene::add_object(Object* obj){
        objects.push_back(obj);
}

void Scene::add_light(Light* lamp){
        lights.push_back(lamp);
}

void Scene::add_tree(float size, vector tree_root){

        int number_of_cones = 3;
        double trunk_size = size/(number_of_cones + 1);
        double trunk_radius = size/7;
        double cones_overage = size/5;
        double big_cone_size = size - trunk_size;
        double big_cone_radius = big_cone_size*0.325; //for a golden triangle
        double part_size = big_cone_size/number_of_cones;

        double sphere_radius = size/30;

        Cylinder *trunk =
                new Cylinder(tree_root, rt::vector(0, -trunk_size, 0),
                             trunk_radius, rt::color(136, 66, 29));
        add_object(trunk);

        for(int i = 0; i < number_of_cones; i++) {
                vector position = tree_root + (trunk_size + i*part_size)
                                  *rt::vector(0,-1,0);
                double proportion = 1. - double(i)/double(number_of_cones);
                double cone_radius = proportion*big_cone_radius + cones_overage;
                double cone_size = proportion*big_cone_size;
                Cone *cone = new Cone(position,
                                      rt::vector(0., -cone_size, 0.),
                                      cone_radius,
                                      color::GREEN);

                vector sphere_position = position +
                                         cone_radius*rt::vector(0,0,-1) +
                                         sphere_radius*rt::vector(0, 1, 0);

                Sphere *sphere = new Sphere(sphere_position, sphere_radius,
                                            color::BLUE, color::RED, 3);

                add_object(cone);
                add_object(sphere);

        }
}

void Scene::add_tree_vidal(float size, vector tree_root){
        double trunk_size = size;
        double trunk_radius = size/7;
        Cylinder *trunk  = new Cylinder(tree_root+rt::vector(0, size, 0),
                                        rt::vector(0, -trunk_size, 0),
                                        trunk_radius, rt::color(136, 66, 29));
        add_object(trunk);
        Cone *cone1 = new Cone(tree_root, rt::vector(0, -size*2, 0),
                               size*5/7, color::GREEN);
        add_object(cone1);
}

void Scene::add_shading(){
        Plane *wall1 = new Plane(rt::vector(0,-700,0)
                                 , rt::vector(0, 1, 0), color::WHITE);
        Plane *wall4 = new Plane(rt::vector(0,700,0),
                                 rt::vector(0, 1, 0), color::WHITE);
        Plane *wall2 = new Plane(rt::vector(-200,0,0),
                                 rt::vector(1, 0, 0), color::BLUE);
        Plane *wall3 = new Plane(rt::vector(0, 0, 700),
                                 rt::vector(0, 0, 1), color::RED);

        Cone *cone1 = new Cone(rt::vector(320, 700, 500),
                               rt::vector(0, -400, 0),200, color::GREEN);
        Sphere *sphere1 = new Sphere(rt::vector(520, 200, 200),
                                     100, orange, brown, 3);
        Cylinder *cyl = new Cylinder(rt::vector(200.,40.,50.),
                                     rt::vector(-1,1,1)*200., 60., yellow);
        Cuboid *cube1 = new Cuboid(rt::vector(450, 450, 300),
                                   rt::vector(-3,1,4).unit()*50,
                                   rt::vector(-1, -15, 3).unit()*50,50,
                                   color::BLUE);
        add_object(cube1);
        add_object(sphere1);
        add_object(wall1);
        add_object(wall2);
        add_object(wall3);
        add_object(wall4);
        add_object(cone1);
        add_object(cyl);

}

void Scene::add_pool(float size, vector table_center){
        float table_length = 275.*size;
        float table_width = 150.*size;
        float table_thickness = 15.*size;
        float border = 10.*size;
        float hole_radius= 3*border/4*size;

        float feet_length = 100.*size;
        float feet_radius = border*size;

        float cue_length = 130.*size;
        float cue_radius = 2.5*size;

        float ball_radius = 5.*size;

        float chalk_size = 2.*size;

        rt::vector table_orientation_l = rt::vector(1., 0., 2.).unit();
        rt::vector table_orientation_w = rt::vector(2., 0., -1.).unit();

        // Table

        Cuboid* table =
                new Cuboid(table_center,
                           table_width * table_orientation_w,
                           table_length * table_orientation_l,
                           table_thickness,
                           brown);
        Rectangle* billiard_cloth =
                new Rectangle(table_center - rt::vector(0.,table_thickness,0.),
                              (table_width-border) * table_orientation_w,
                              (table_length-border) * table_orientation_l,
                              grass_green);

        // Let's put some holes!!
        Disk* first_hole = new Disk(table_center
                                    - rt::vector(0., table_thickness,0.)
                                    - (table_length-border)*table_orientation_l
                                    + (table_width-border)*table_orientation_w,
                                    rt::vector(0., -1., 0.),
                                    hole_radius,
                                    color::BLACK);

        Disk* second_hole = new Disk(table_center
                                     - rt::vector(0., table_thickness,0.)
                                     - (table_length-border)*table_orientation_l
                                     - (table_width-border)*table_orientation_w,
                                     rt::vector(0., -1., 0.),
                                     hole_radius,
                                     color::BLACK);
        Disk* third_hole = new Disk(table_center
                                    - rt::vector(0., table_thickness,0.)
                                    + (table_length-border)*table_orientation_l
                                    - (table_width-border)*table_orientation_w,
                                    rt::vector(0., -1., 0.),
                                    hole_radius,
                                    color::BLACK);
        Disk* fourth_hole = new Disk(table_center
                                     - rt::vector(0., table_thickness,0.)
                                     + (table_length-border)*table_orientation_l
                                     + (table_width-border)*table_orientation_w,
                                     rt::vector(0., -1., 0.),
                                     hole_radius,
                                     color::BLACK);
        Disk* fifth_hole = new Disk(table_center
                                    - rt::vector(0., table_thickness,0.)
                                    + (table_width-border)*table_orientation_w,
                                    rt::vector(0., -1., 0.),
                                    hole_radius,
                                    color::BLACK);
        Disk* sixth_hole = new Disk(table_center
                                    - rt::vector(0., table_thickness,0.)
                                    - (table_width-border)*table_orientation_w,
                                    rt::vector(0., -1., 0.),
                                    hole_radius,
                                    color::BLACK);

        // It's looks better with feets
        Cylinder* first_feet = new Cylinder(table_center
                                            + rt::vector(0., table_thickness,0.)
                                            - (table_length-border)
                                            *table_orientation_l
                                            + (table_width-border)
                                            *table_orientation_w,
                                            feet_length *
                                            rt::vector(0., 1., 0.),
                                            feet_radius,
                                            brown);
        Cylinder* second_feet = new Cylinder(table_center
                                             + rt::vector(0.,
                                                          table_thickness,0.)
                                             - (table_length-border)
                                             *table_orientation_l
                                             - (table_width-border)
                                             *table_orientation_w,
                                             feet_length
                                             * rt::vector(0., 1., 0.),
                                             feet_radius,
                                             brown);
        Cylinder* third_feet = new Cylinder(table_center
                                            + rt::vector(0., table_thickness,0.)
                                            + (table_length-border)
                                            *table_orientation_l
                                            - (table_width-border)
                                            *table_orientation_w,
                                            feet_length
                                            * rt::vector(0., 1., 0.),
                                            feet_radius,
                                            brown);
        Cylinder* fourth_feet = new Cylinder(table_center
                                             + rt::vector(0.,
                                                          table_thickness,0.)
                                             + (table_length-border)
                                             *table_orientation_l
                                             + (table_width-border)
                                             *table_orientation_w,
                                             feet_length
                                             * rt::vector(0., 1., 0.),
                                             feet_radius,
                                             brown);
        Cylinder* fifth_feet = new Cylinder(table_center
                                            + rt::vector(0., table_thickness,0.)
                                            + (table_width-border)
                                            *table_orientation_w,
                                            feet_length
                                            * rt::vector(0., 1., 0.),
                                            feet_radius,
                                            brown);
        Cylinder* sixth_feet = new Cylinder(table_center
                                            + rt::vector(0., table_thickness,0.)
                                            - (table_width-border)
                                            *table_orientation_w,
                                            feet_length
                                            * rt::vector(0., 1., 0.),
                                            feet_radius,
                                            brown);

        // And a ground !

        Plane* ground = new Plane(table_center
                                  + rt::vector(0., table_thickness,0.)
                                  + feet_length * rt::vector(0., 1., 0.),
                                  rt::vector(0., -1, 0.),
                                  color::WHITE);


        // You will need a cue to play !

        Cylinder* cue = new Cylinder(table_center
                                     - rt::vector(0.,
                                                  table_thickness +
                                                  cue_radius,0.)
                                     - (table_width/2)*table_orientation_w
                                     - (3*table_length/4)*table_orientation_l,
                                     cue_length * (table_orientation_w
                                                   +table_orientation_l).unit(),
                                     cue_radius,
                                     brown);

        // And some balls !

        Sphere* ball_1 = new Sphere(table_center
                                    - rt::vector(0., table_thickness
                                                 + ball_radius,0.)
                                    - (table_width/2)*table_orientation_w
                                    + (table_length/2)*table_orientation_l,
                                    ball_radius,
                                    dark_blue,
                                    color::WHITE,
                                    2);
        Sphere* ball_3 = new Sphere(table_center
                                    - rt::vector(0., table_thickness
                                                 + ball_radius,0.)
                                    + (table_width/2)*table_orientation_w
                                    - (3*table_length/4)*table_orientation_l,
                                    ball_radius,
                                    color::RED);
        Sphere* ball_9 = new Sphere(table_center
                                    - rt::vector(0., table_thickness
                                                 + ball_radius,0.)
                                    + (table_width-2*border)*table_orientation_w
                                    + (3*table_length/4)*table_orientation_l,
                                    ball_radius,
                                    yellow);
        Sphere* ball_13 = new Sphere(table_center
                                     - rt::vector(0., table_thickness
                                                  + ball_radius,0.)
                                     + (3*table_width/4)*table_orientation_w
                                     - (table_length - 2*border)
                                     *table_orientation_l,
                                     ball_radius,
                                     orange,
                                     color::WHITE,
                                     2);

        Sphere* players_ball = new Sphere(table_center
                                          - rt::vector(0., table_thickness
                                                       + ball_radius,0.),
                                          ball_radius,
                                          color::WHITE);

        // You can even use some chalk if you want !

        Cuboid* chalk = new Cuboid(table_center
                                   - rt::vector(0., table_thickness,0.)
                                   + (table_width - border/2)
                                   * table_orientation_w
                                   - (3*table_length/4) * table_orientation_l,
                                   chalk_size * rt::vector(1.,0.,0.),
                                   chalk_size * rt::vector(0.,1.,0.),
                                   chalk_size,
                                   color::BLUE);


        add_object(first_hole);
        add_object(second_hole);
        add_object(third_hole);
        add_object(fourth_hole);
        add_object(fifth_hole);
        add_object(sixth_hole);

        add_object(first_feet);
        add_object(second_feet);
        add_object(third_feet);
        add_object(fourth_feet);
        add_object(fifth_feet);
        add_object(sixth_feet);

        add_object(billiard_cloth);
        add_object(table);

        add_object(ground);
        add_object(cue);
        add_object(chalk);

        add_object(ball_1);
        add_object(ball_3);
        add_object(ball_9);
        add_object(ball_13);

        add_object(players_ball);
}
