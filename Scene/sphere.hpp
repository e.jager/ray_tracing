/*!
 * \file sphere.hpp
 * \brief Plane class
 * \author Vidal Attias, Clément Legrand-Duchesne, Valentin Taillandier
 * \version 0.1
 */

#pragma once

#include "color.hpp"
#include "vector.hpp"
#include "hit.hpp"
#include "ray.hpp"
#include "object.hpp"
#include "../Core/math.hpp"

using namespace rt;

/*! \class Rectangle
 * \brief Class representing a rectangle.
 *
 * The Sphere class represents a sphere through the position of
 * the center of the rectangle, a radius, a color (and potentially a
 * color for the strip).
 */

class Sphere : public Object {
private:
vector center;   /*!< Vector representing the position of the center
                    of the sphere. */
double radius;   /*!< Radius of the sphere*/
color col;   /*!< Main color of the sphere. */
color col_strip;   /*!< Color of the strips. */
int strips;   /*!<Represents the number of strips. 0 for a
                 monochromatic sphere. */
public:
/*!
 * \brief Constructor: builds a sphere.
 *
 * \param c The position of the center of the sphere.
 * \param r The radius.
 * \param co The color.
 *
 **Creates a monochromatic sphere
 */
Sphere(vector c, double r, color co);

/*!
 * \brief Constructor: builds a sphere.
 *
 * \param c The position of the center of the sphere.
 * \param r The radius.
 * \param co The color.
 * \param co2 The color of the strips.
 * \param s The number of strips.
 *
 **Creates a bichromatic sphere
 */
Sphere(vector c, double r, color co, color co2, int s);

/*! \fn vector get_center() const
 * \brief Gets the center of the sphere.
 */
vector get_center() const;

/*! \fn double get_radius() const
 * \brief Gets the radiusof the sphere.
 */
double get_radius() const;

/*! \fn color get_color(vector_point) const
 * \brief Gets the color of the point.
 * \param point The point of the surface.
 */
color get_color(vector point) const;

/*!
 * \fn  double send(Ray r) const
 *
 * \brief Finds the distance to the intersection of the
 * ray with the sphere.
 *
 * \param r A ray.
 *
 * \return A double t, t being the distance from the origin of the
 * ray to the sphere in the ray direction. t is strictly positiv. If
 * the ray doesn't cross the sphere then t = infinity.
 */
double send(Ray r) const;

/*!
 * \fn  Hit intersect(Ray r, double t) const
 *
 * \brief Creates the resulting Hit.
 *
 * \param r A ray.
 * \param t A double.
 *
 * \return A Hit.
 *
 * Creates the Hit corresponding to the intersection of the sphere
 * and the ray at a distance t times the direction of the ray from
 * the origin of the ray.
 * Computes the normal vector to the surface crossed and its color.
 */
Hit intersect(Ray r, double t) const;
};
