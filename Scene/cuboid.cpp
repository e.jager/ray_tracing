#include "cuboid.hpp"

using namespace rt;

Cuboid::Cuboid(vector p, vector dir1, vector dir2,double hei, color co)
{
  position = p;
  direction1 = dir1;
  direction1 = project_line(dir2,dir1);
  height = hei;
  vector dir3 = (dir1^dir2).unit()*hei;
  // Each face of the cuboid are declared below
  rectangles.push_back(Rectangle(p + dir1, dir2, dir3, co));
  rectangles.push_back(Rectangle(p - dir1, dir2, dir3, co));
  rectangles.push_back(Rectangle(p + dir2, dir1, dir3, co));
  rectangles.push_back(Rectangle(p - dir2, dir1, dir3, co));
  rectangles.push_back(Rectangle(p + dir3, dir1, dir2, co));
  rectangles.push_back(Rectangle(p - dir3, dir1, dir2, co));
}

vector Cuboid::get_position() const {
        return position;
}

vector Cuboid::get_first_direction() const {
        return direction1;
}


vector Cuboid::get_second_direction() const {
        return direction2;
}


double Cuboid::get_height() const {
        return height;
}


color Cuboid::get_color() const {
        return col;
}

std::pair<double, int> Cuboid::send_which(Ray r) const {

        double t = infinity;
        int touched = 0;
        double t_temp;

	// Find the face hit by the ray
        for(int i = 0; i < 6; i++) {
                t_temp = rectangles.at(i).send(r);
                if(t_temp < t) {
                        t = t_temp;
                        touched = i;
                }
        }

        const double t_final = t;
        const int touched_final = touched;

        return std::pair<double, int>(t_final, touched_final);
}

double Cuboid::send(Ray r) const {
        return send_which(r).first;
}

Hit Cuboid::intersect(Ray r, double t) const {
        int touched = send_which(r).second;
        return rectangles.at(touched).intersect(r, t);

}
