#include "cylinder.hpp"

using namespace rt;

Cylinder::Cylinder(vector p, vector a, double r, color co) :
        bottom(Disk(p, a, r, co)), top(Disk((a+p), a, r, co)){
        position = p;
        axis = a;
        radius = r;
        col = co;
}

vector Cylinder::get_position() const {
        return position;
}

vector Cylinder::get_axis() const {
        return axis;
}

double Cylinder::get_radius() const {
        return radius;
}

color Cylinder::get_color() const {
        return col;
}

std::pair<double, char> Cylinder::send_which(Ray r) const {
        vector dir = r.get_direction().unit();
        vector orig = r.get_origin();
        vector dir_proj = project_plane(dir, axis);
        vector orig_proj = project_plane(orig, axis);
        vector pos_proj = project_plane(position, axis);
        vector ac = pos_proj - orig_proj;
        double a = dir_proj.norm()*dir_proj.norm();
        double b = -2*(ac|dir_proj);
        double c = ac.norm()*ac.norm() - radius*radius;

        // t_cyl is the distance to the infinite cylindrical hull
        double t_cyl = solve2(a,b,c);

        if(t_cyl < 0) {
                t_cyl = infinity;
        } //redundant


        if(t_cyl < infinity) {
                vector h =
                        ((orig - position + t_cyl*dir)|axis.unit())*axis.unit()
                        - axis*0.5;
                if (h.norm() > axis.norm()/2) {
                        t_cyl = infinity;
                }
        }

        double t_top = top.send(r);
        double t_bot = bottom.send(r);

        // WARNING: as t_cyl is the first positiv root, it is possible that
        // it corresponds to a point that above or below the bases while the
        // second root is a correct match. Resulting in a hole in the
        // cylinder That is actually not a serious matter because this hole
        // will be hidden by the disks in the eyes of the observater and
        // will not spoil the shades
        double t;
        char touched;

        if (t_cyl < t_bot) {
                if (t_top < t_cyl) {
                        touched = 't';
                        t = t_top;
                }
                else{
                        touched = 'c';
                        t = t_cyl;
                }
        }
        else{
                if (t_top < t_bot) {
                        touched = 't';
                        t = t_top;
                }
                else{
                        touched = 'b';
                        t = t_bot;
                }
        }

        const double t_final = t;
        const char touched_final = touched;

        return std::pair<double, char>(t_final, touched_final);
}

double Cylinder::send(Ray r) const {
        return send_which(r).first;
}

Hit Cylinder::intersect(Ray r, double t) const {
        char touched = send_which(r).second;
        vector dir = r.get_direction().unit();
        vector orig = r.get_origin();
        double intersect_x = orig.x + t*dir.x;
        double intersect_y = orig.y + t*dir.y;
        double intersect_z = orig.z + t*dir.z;
        vector intersect_point = vector(intersect_x, intersect_y, intersect_z);
        vector normal_vector = vector();
        if (touched == 'c') {
                normal_vector =
                        (project_plane(intersect_point -
                                       position, axis)).unit();
                return Hit(r, intersect_point, normal_vector, col);
        }
        else if (touched == 't') {
                return top.intersect(r, t);
        }
        return bottom.intersect(r, t);
}
