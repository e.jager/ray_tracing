#include "engine.hpp"

using namespace rt;

Engine::Engine() :
        width(640), height(480) {
}

Engine::Engine(int w, int h) :
        width(w), height(h) {
}

color Engine::pixel_color(double i, double j, Scene universe){
        Camera cam = universe.get_camera();
        std::vector <Object*> objects = universe.get_objects();
        vector pixel_position = vector((double) i,(double) j, 0.);
        //arbitraire pour l'instant
        vector cam_position = cam.get_position();
        vector dir = (pixel_position - cam_position).unit();
        Ray current_ray = Ray(cam_position, dir);
        double t = infinity;
        double temp_t;
        Object* first_object_met = nullptr;
        std::vector <Object*> met_objects;

        for(std::vector <Object*>::iterator it = objects.begin();
            it != objects.end();
            it++) {
                temp_t = (**it).send(current_ray);
                if (temp_t < t) {
                        t = temp_t;
                        first_object_met = *it;
                }
        }


        met_objects.push_back(first_object_met);

        color pixel_temp = color::BLACK;
        if(t<infinity) {
                Object* second_object_met = nullptr;
                Hit intersect = (*first_object_met).intersect(current_ray, t);
                vector n = intersect.get_normal();
                vector a = current_ray.get_direction();

                vector new_direction = project_plane(a,n)-project_line(a, n);
                vector new_point = intersect.get_point();

                Ray new_ray = Ray(new_point, new_direction);
                double new_temp_t;

                for(std::vector <Object*>::iterator it = objects.begin();
                    it != objects.end();
                    it++) {
                        if((*it)!= first_object_met) {
                                new_temp_t = (**it).send(new_ray);
                                if (new_temp_t < t) {
                                        second_object_met = *it;
                                }
                        }
                }
                met_objects.push_back(second_object_met);

                if (second_object_met != nullptr) {
                        Hit h =
                                (*second_object_met).intersect(new_ray,
                                                               new_temp_t);
                        vector point = h.get_point();
                        std::vector <Light*> lights =  universe.get_lights();

                        for(std::vector <Light*>::iterator lamp_ptr =
                                    lights.begin();
                            lamp_ptr != lights.end();
                            lamp_ptr++) {

                                Light lamp = **lamp_ptr;
                                vector to_light = lamp.get_position() - point;
                                Ray ray_to_light = Ray(point, to_light);

                                bool obstacle = false;
                                double temp_t2;
                                for(std::vector <Object*>::iterator it2
                                            = objects.begin();

                                    it2 != objects.end();
                                    it2++) {
                                        if (*it2 != second_object_met) {
                                                temp_t2 =
                                                        (**it2)
                                                        .send(ray_to_light);
                                                if (temp_t2 < to_light.norm()) {
                                                        obstacle = true;
                                                }

                                        }


                                }



                                if(not obstacle) {
                                        pixel_temp = pixel_temp + lamp.apply(h);
                                }
                        }
                }
                else{
                        pixel_temp = pixel_temp + color::BLACK;
                }
        }

        color pixel_col = color::BLACK;

        if (first_object_met != nullptr) {
                Hit h = (*first_object_met).intersect(current_ray, t);
                vector point = h.get_point();
                std::vector <Light*> lights =  universe.get_lights();

                for(std::vector <Light*>::iterator lamp_ptr = lights.begin();
                    lamp_ptr != lights.end();
                    lamp_ptr++) {

                        Light lamp = **lamp_ptr;
                        vector to_light = lamp.get_position() - point;
                        Ray ray_to_light = Ray(point, to_light);

                        bool obstacle = false;
                        double temp_t2;
                        for(std::vector <Object*>::iterator it2
                                    = objects.begin();

                            it2 != objects.end();
                            it2++) {
                                if (*it2 != first_object_met) {
                                        temp_t2 = (**it2).send(ray_to_light);
                                        if (temp_t2 < to_light.norm()) {
                                                obstacle = true;
                                        }

                                }


                        }



                        if(not obstacle) {
                                pixel_col = pixel_col + lamp.apply(h);
                        }
                }
        }
        else{
                pixel_col = pixel_col + color::BLACK;
        }

        float coef_reflexion = 0.2;

        pixel_col = (pixel_col*(1-coef_reflexion))+(pixel_temp*coef_reflexion);
        return pixel_col;
}

void Engine::render_scene(Scene universe, int definition){
        int def = definition;
        screen img = screen(width,height);
        color color1;
        color color2;
        color color3;
        color color4;
        for (int i = 0; i < width*def; i++) {
                for (int j = 0; j < height*def; j++) {
                        //Antialiasing x4
                        color1 = pixel_color(i/def,j/def, universe);
                        color2 = pixel_color((i + 0.5)/def, (j + 0.5)/def, universe);
                        color3 = pixel_color((i + 0.5)/def,j/def, universe);
                        color4 = pixel_color(i/def, (j + 0.5)/def, universe);
                        img.set_pixel(i, j,
                                      fuse(color1, color2, color3, color4));
                }
        }
        img.update();
}
