#pragma once

#include "vector.hpp"
#include "color.hpp"

using namespace rt;

class Object {

private:
  color col;
  double diffuse = 50.0;
  double specular = 50.0;
  double specular_exponent = 5.0;

public:
  /* Pure virtual method */
  virtual Hit intersect(const Ray r, const double t) const= 0;
  // /* Pure virtual method */
  // virtual vector get_normal(const vector& intersection) const = 0;
  /* Pure virtual method */
  virtual double send(Ray r) const= 0;

  // Get methods
  color get_color() const {
    return col;
  }
  double get_diffuse() const {
    return diffuse;
  }
  double get_specular() const {
    return specular;
  }
  double get_specular_exponent() const {
    return specular_exponent;
  }

  // Set methods
  void set_color(color c) {
    col = c;
  }
  void set_diffuse(double diff) {
    diffuse = diff;
  }
  void set_specular(double spec) {
    specular = spec;
  }
  void set_specular_exponent(double spec_ex) {
    specular_exponent = spec_ex;
  }

  // General set method
  void set_appearance(color c, double diff, double spec, double spec_ex) {
    col = c;
    diffuse = diff;
    specular = spec;
    specular_exponent = spec_ex;
  }
};
