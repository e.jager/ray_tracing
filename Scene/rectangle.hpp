/*!
 * \file rectangle.hpp
 * \brief Plane class
 * \author Vidal Attias, Clément Legrand-Duchesne, Valentin Taillandier
 * \version 0.1
 */

#pragma once

#include "color.hpp"
#include "vector.hpp"
#include "hit.hpp"
#include "ray.hpp"
#include "object.hpp"
#include "math.hpp"

using namespace rt;

/*! \class Rectangle
 * \brief Class representing a rectangle.
 *
 * The Rectangle class represents a rectangle through the position of
 * the center of the rectangle, two vectors direction, and a color.
 */
class Rectangle : public Object {

private:
  vector position; /*!< Vector representing the position of the
                       center of the rectangle. */
  vector direction1; /*!< Vector leading from the center of the
                        rectangle to one edge e1. */
  vector direction2; /*!< Vector leading from the center of the
                        rectangle to an edge e2 next to e1. */
  vector normal; /*!< Vector leading from the center of the
                        rectangle to an edge e2 next to e1. */
  color col; /*!< Color of the rectangle. */

public:

   /*!
   * \brief Constructor: builds a cuboid.
   *
   * \param p The position of the center of the rectangle.
   * \param dir1 The first direction.
   * \param dir2 The second direction.
   * \param co The color.
   *
   * \warning The directions given in parameters has to be
   * orthogonal. Otherwise the vector taken as direction2 will be the
   * projection the second direction on the plane otrhogonal to the
   * first one.
   */
  Rectangle(vector p, vector dir1, vector dir2, color co);

  /*! \fn vector get_position() const
   * \brief Gets the position of the center.
   */
  vector get_position() const;


  /*! \fn vector get_first_direction() const
   * \brief Gets the first direction.
   */
  vector get_first_direction() const;

  /*! \fn vector get_second_direction() const
   * \brief Gets the second direction.
   */
  vector get_second_direction() const;

  /*! \fn color get_color() const
   * \brief Gets the color.
   */
  color get_color() const;

  /*! \fn vector get_normal() const
   * \brief Gets a vector normal to the surface.
   */
  vector get_normal() const;

  /*!
   * \fn  double send(Ray r) const
   *
   * \brief Finds the distance to the intersection of the
   * ray with the rectangle.
   *
   * \param r A ray.
   *
   * \return A double t, t being the distance from the origin of the
   * ray to the rectangle in the ray direction. t is strictly positiv. If
   * the ray doesn't cross the rectangle then t = infinity.
   */
   double send(Ray r) const;

  /*!
   * \fn  Hit intersect(Ray r, double t) const
   *
   * \brief Creates the resulting Hit.
   *
   * \param r A ray.
   * \param t A double.
   *
   * \return A Hit.
   *
   * Creates the Hit corresponding to the intersection of the rectangle
   * and the ray at a distance t times the direction of the ray from
   * the origin of the ray.
   * Computes the normal vector to the surface crossed and its color.
   */
   Hit intersect(Ray r, double t) const;
};
