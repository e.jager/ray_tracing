/*!
 * \file scene.hpp
 * \brief Scene class
 * \author Vidal Attias, Clément Legrand-Duchesne, Valentin Taillandier
 * \version 0.1
 */


#pragma once


#include <vector>


#include "vector.hpp"
#include "screen.hpp"
#include "image.hpp"
#include "color.hpp"

#include "sphere.hpp"
#include "cylinder.hpp"
#include "disk.hpp"
#include "rectangle.hpp"
#include "cuboid.hpp"
#include "cone.hpp"
#include "plane.hpp"

#include "ray.hpp"
#include "hit.hpp"
#include "light.hpp"
#include "object.hpp"
#include "camera.hpp"

using namespace rt;


/*! \class Scene
 * \brief Class representing a scene which can
 * contain objects, lights and camera
 *
 * The engine is set by the width and the eight of the graphic needed.
 * The render_scene method computes the graphic from a Scene object.
 * color. */
class Scene {
private:
std::vector<Object*> objects; /*!< std::vector<Object*> representing
                                 the objects list. */
std::vector<Light*> lights; /*!< std::vector<Light*> representing
                                   the lights list. */
Camera cam; /*!< Camera representing the camera. */

public:

/*!
 * \brief Constructor: builds a Scene.
 *
 * \param cam The camera.
 */
Scene(Camera cam);
Scene();

/*! \fn Camera get_camera() const;
 * \brief Returns the camera of the scene.
 */
Camera get_camera() const;

/*! \fn std::vector <Object*> get_objects() const;
 * \brief Returns the objects list.
 */
std::vector <Object*> get_objects() const;

/*! \fn std::vector <Light*> get_lights() const;
 * \brief Returns the lights list.
 */
std::vector <Light*> get_lights() const;

/*! \fn void add_object(Object* obj);
 * \brief Adds an object to the scene.
 * \param obj The Object pointer.
 */
void add_object(Object* obj);

/*! \fn void add_light(Light* lamp);
 * \brief Adds a light to the scene.
 * \param lamp The Light pointer.
 */
void add_light(Light* lamp);

/*! \fn void add_pool(float size, vector table_center);
 * \brief Adds a pool to the scene.
 * \param size The size of the pool.
 * \param table_center The position of the pool.
 */
void add_pool(float size, vector table_center);

/*! \fn void add_tree(float size, vector tree_root);
 * \brief Adds a christmas tree to the scene.
 * \param size The size of tree.
 * \param table_center The position of the tree.
 */
void add_tree(float size, vector tree_root);
void add_tree_vidal(float size, vector tree_root);

void add_shading();
};
