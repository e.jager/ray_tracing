/*!
 * \file disk.hpp
 * \brief Disk class
 * \author Vidal Attias, Clément Legrand-Duchesne, Valentin Taillandier
 * \version 0.1
 */

#pragma once

#include "color.hpp"
#include "vector.hpp"
#include "hit.hpp"
#include "ray.hpp"
#include "object.hpp"

using namespace rt;

/*! \class Disk
 * \brief Class representing a disk.
 *
 * The Disk class represents a disk through the position of
 * its center, a vector normal, a radius, and a color.
 */
class Disk : public Object {
private:
vector position;   /*!< Vector representing the position of the
                      center */
vector normal;   /*!< Vector normal to the disk */
double radius;   /*!< Radius of the disk */
color col;   /*!< Color of the disk */

public:

/*!
 * \brief Constructor: builds a disk.
 *
 * \param p The position of its center.
 * \param n The normal vector.
 * \param r The radius.
 * \param co The color.
 */
Disk(vector p, vector n, double r, color co);

/*! \fn vector get_position() const
 * \brief Gets the position of the center.
 */
vector get_position() const;

/*! \fn vector get_normal() const
 * \brief Gets the vector normal to the surface.
 */
vector get_normal() const;

/*! \fn color get_color() const
 * \brief Gets the color of the disk.
 */
color get_color() const;

/*! \fn double get_radius() const
 * \brief Gets the radius of the disk.
 */
double get_radius() const;

/*!
 * \fn  double send(Ray r) const
 *
 * \brief Finds the distance to the intersection of the
 * ray with the disk.
 *
 * \param r A ray.
 *
 * \return A double t, t being the distance from the origin of the
 * ray to the disk in the ray direction. t is strictly positiv. If
 * the ray doesn't cross the disk then t = infinity.
 */
double send(Ray r) const;

/*!
 * \fn  Hit intersect(Ray r, double t) const
 *
 * \brief Creates the resulting Hit.
 *
 * \param r A ray.
 * \param t A distance.
 *
 * \return A Hit.
 *
 * Creates the Hit corresponding to the intersection of the disk
 * and the ray at a distance t times the direction of the ray from
 * the origin of the ray.
 * Computes the normal vector to the surface crossed and its color.
 */
Hit intersect(Ray r, double t) const;
};
